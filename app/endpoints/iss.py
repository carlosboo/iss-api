from typing import List
from datetime import datetime, timedelta
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.db.db import ISSData
from app.db.db_utils import get_db


router = APIRouter()


def calculate_sun_exposure(data):
    '''
    Calculate sun exposure intervals
    :param data:
    :return:
    '''
    intervals = []
    current_interval = None

    for record in data:
        if record.visibility == "daylight":
            if current_interval is None:
                current_interval = {
                    "start_time": datetime.utcfromtimestamp(record.timestamp).strftime("%d-%m-%Y %H:%M:%S"),
                    "end_time": None
                }
        else:
            if current_interval:
                current_interval["end_time"] = datetime.utcfromtimestamp(record.timestamp).strftime("%d-%m-%Y %H:%M:%S")
                intervals.append(current_interval)
                current_interval = None

    if current_interval:
        current_interval["end_time"] = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        intervals.append(current_interval)

    return intervals


@router.get("/sun", response_model=List[dict])
def get_sun_exposure(db: Session = Depends(get_db)):
    '''
    Endpoint that returns sun exposure intervals from all the database data
    :param db:
    :return:
    '''
    today = datetime.utcnow()
    one_month_ago = today - timedelta(days=30)

    data = db.query(ISSData).filter(ISSData.timestamp >= one_month_ago).order_by(ISSData.timestamp).all()

    for record in data:
        record.timestamp = record.timestamp.timestamp()

    sun_exposure_intervals = calculate_sun_exposure(data)

    return sun_exposure_intervals


@router.get("/position", response_model=dict)
def get_iss_position(db: Session = Depends(get_db)):
    '''
    Endpoint that returns currently ISS position
    :param db:
    :return:
    '''
    # Get most recent data from db
    latest_data = db.query(ISSData).order_by(ISSData.timestamp.desc()).first()

    if latest_data:
        # Actual ISS position
        iss_position = {
            "latitude": latest_data.latitude,
            "longitude": latest_data.longitude,
        }
        return iss_position
    else:
        return {"error": "No ISS position data found in the database"}

