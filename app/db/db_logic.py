import asyncio
import requests
from datetime import datetime, timedelta
from app.db.db import ISSData

ISS_API_URL = "https://api.wheretheiss.at/v1/satellites/25544"


def insert_or_update_iss_data(db, iss_data):
    '''
    Saving ISS data in the database
    :param db:
    :param iss_data:
    :return:
    '''
    existing_data = db.query(ISSData).filter(
        ISSData.id == iss_data['id'],
        ISSData.timestamp == iss_data['timestamp']
    ).first()
    if existing_data:
        # if exists update
        for key, value in iss_data.items():
            setattr(existing_data, key, value)
    else:
        # ...if not update
        db_data = ISSData(**iss_data)
        db.add(db_data)
    db.commit()


async def fetch_and_store_iss_data(db):
    '''
    ISS info data request each 20 secs
    :param db:
    :return:
    '''
    while True:
        await asyncio.sleep(20)  # Mover la espera al principio del bucle
        response = requests.get(ISS_API_URL)
        if response.status_code == 200:
            iss_data = response.json()
            # datetime conversion
            iss_data['timestamp'] = datetime.utcfromtimestamp(iss_data['timestamp'])
            insert_or_update_iss_data(db, iss_data)


def load_initial_data(db):
    '''
    Load iss data from the last 10 hours to fill the sun exposure intervals
    :param db:
    :return:
    '''
    timestamps = [int((datetime.utcnow() - timedelta(hours=i)).timestamp()) for i in range(20)]
    positions_url = f"{ISS_API_URL}/positions?timestamps={','.join(map(str, timestamps))}&units=miles"
    response = requests.get(positions_url)

    if response.status_code == 200:
        iss_positions = response.json()

        for position in iss_positions:
            position['timestamp'] = datetime.utcfromtimestamp(position['timestamp'])
            insert_or_update_iss_data(db, position)

