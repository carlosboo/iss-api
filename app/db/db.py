from sqlalchemy import Column, Integer, String, Float, DateTime
from sqlalchemy import create_engine
from sqlalchemy import PrimaryKeyConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from datetime import datetime

DATABASE_URL = "sqlite:///./mydatabase.db"
engine = create_engine(DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class ISSData(Base):
    __tablename__ = "iss_data"

    id = Column(Integer, index=True)
    name = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)
    altitude = Column(Float)
    velocity = Column(Float)
    visibility = Column(String)
    footprint = Column(Float)
    timestamp = Column(DateTime, default=datetime.utcnow)
    daynum = Column(Float)
    solar_lat = Column(Float)
    solar_lon = Column(Float)
    units = Column(String)

    # Define a composite primary key using PrimaryKeyConstraint
    __table_args__ = (
        PrimaryKeyConstraint('id', 'timestamp'),
    )
