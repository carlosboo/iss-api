import json
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_get_iss_position():
    response = client.get("/iss/position")
    assert response.status_code == 200
    data = response.json()
    assert "latitude" in data
    assert "longitude" in data


def test_get_sun_exposure():
    response = client.get("/iss/sun")
    assert response.status_code == 200
    data = response.json()
    assert isinstance(data, list)
    for interval in data:
        assert "start_time" in interval
        assert "end_time" in interval
