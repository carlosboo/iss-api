import uvicorn
import threading
import asyncio
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.endpoints import iss
from app.db.db_logic import fetch_and_store_iss_data, load_initial_data
from app.db.db import Base
from app.db.db_utils import get_db
from app.db.db import engine

app = FastAPI()

# CORS config
origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# bd config
Base.metadata.create_all(bind=engine)

# ISS router register
app.include_router(iss.router, prefix="/iss")

# Load initial data on startup
db = next(get_db())
load_initial_data(db)


def run_fetch_and_store_iss_data():
    asyncio.set_event_loop(asyncio.new_event_loop())
    loop = asyncio.get_event_loop()
    db = next(get_db())
    loop.run_until_complete(fetch_and_store_iss_data(db))


if __name__ == "__main__":
    print("Starting execution main.py", flush=True)

    fetch_thread = threading.Thread(target=run_fetch_and_store_iss_data)
    fetch_thread.start()

    uvicorn.run(app, host="0.0.0.0", port=8000)
