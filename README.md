# ISS Project

Project that leverages FastAPI to create an API for retrieving and storing International Space Station (ISS) data. This README provides instructions for setting up the project, including creating a virtual environment and installing dependencies.

## Prerequisites

Before getting started, make sure you have the following installed on your system:

- [Python](https://www.python.org/downloads/)
- [pip](https://pip.pypa.io/en/stable/installation/)

## Getting Started

Follow these steps to set up the project:

### 1. Clone the Repository

Clone the repository to your local machine

### 2. Create a Virtual Environment

Navigate to the project directory and create a virtual environment using venv or virtualenv:

    cd iss-api
    python -m venv venv

### 3. Activate the Virtual Environment

Activate the virtual environment:

    source venv/bin/activate

### 4. Install Dependencies

Install the project dependencies using pip:

    pip install -r requirements/dev.txt

### 5. Database

This project uses a SQLite database by default. You can modify the database settings in app/db/db.py if needed.


### 6. Tests

You can run unit tests to verify the functionality of the API endpoints. Make sure you have your virtual environment activated, and then run the following command:

    pytest

This will execute the tests in the tests directory to ensure that the API is working as expected.

### 7. Run the Application

You can run the application using Uvicorn:

    python main.py

The API will be accessible at http://localhost:8000/.


## Usage

To use the API endpoints and access the ISS data, you can make requests to the following routes:

    http://localhost:8000/iss/position: Get the current position of the ISS.
    http://localhost:8000/iss/sun: Get the intervals when the ISS was exposed to sunlight in the last month.
